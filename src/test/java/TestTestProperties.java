import org.junit.Test;
import other.TestProperties;

import java.util.Properties;
public class TestTestProperties {
    private Properties props = TestProperties.getInstance().getProperties();
    @Test
    public  void printProps(){
        props.forEach((key,value)-> System.out.printf("props: %s, value %s\n", key, value));
    }
}
