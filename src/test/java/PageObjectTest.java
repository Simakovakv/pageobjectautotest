/**
 * 1.	Перейдите на сервис http://www.ozon.ru/
 * 2.	Выполните авторизацию на сервисе с ранее созданным логином и паролем
 * 3.	Выполните поиск по «iPhone 7 Plus/8 Plus Black»
 * 4.	Из результатов поиска добавьте в корзину 5 товаров.
 * 5.	Перейдите в корзину, убедитесь, что все добавленные ранее товары находятся в корзине
 * 6.	Удалите все товары из корзины
 * 7.	Разлогиньтесь с сервиса
 * 8.	Выполните авторизацию на сервисе
 * 9.	Проверьте, что корзина не содержит никаких товаров
 */

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import pages.Init;
import steps.MainSteps;

import java.io.File;
import java.io.IOException;

public class PageObjectTest {
    @Before
    public void start() {
        Init.startUp();
    }

    @After
    public void shutDown() {
        Init.shutDown();
    }
    @Ignore("for now, because it's previos test")
    @Test
    public void testPageObject() throws IOException {
        MainSteps mainSteps = new MainSteps();
        mainSteps.goToTheSource()
                .loginToService()
                .searchProduct()
                .addItemsToBasket(5)
                .goToBasketAndCheckItems()
                .deleteAllItems()
                .logoutFromService()
                .loginToService()
                .goToBasketAndCheckItems();

        System.out.println(Init.getWebDriver().getCurrentUrl());
        File scrFile = ((TakesScreenshot) Init.getWebDriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("lastscreen.png"));
    }

    @Test
    public void testPageObject2() throws IOException {
        MainSteps mainSteps = new MainSteps();
        mainSteps.goToTheSource()
                .chooseCategoryFromMenu("Электроника", "Смартфоны")
                .clickOneMoreButton()
                .chooseBrendOfItem("Apple")
                .enterToFieldStartPrice(50000)
                .addItemsToBasket(1)
                .goToBasketAndCheckItems()
                .deleteAllItems()
                .checkItems();
    }
    @Test
    public void testPageObject3() {
        MainSteps mainSteps = new MainSteps();
        mainSteps.goToTheSource()
                .chooseCategoryFromMenu("Электроника", "Фотокамеры")
                .clickOneMoreButton()
                .chooseBrendOfItem("Nikon")
                .chooseBrendOfItem("Canon")
                .enterToFieldStartPrice(80000)
                .addItemsToBasket(1)
                .goToBasketAndCheckItems()
                .deleteAllItems()
                .goToBasketAndCheckItems();
    }
}




