import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import steps.MyStepdefs;
import steps.MyStepdefs;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = { "src/test/resources/features" },
        glue = {"steps"},
        tags = {"@all"},
        plugin = {"ru.yandex.qatools.allure.cucumberjvm.AllureReporter"}

)
public class CucumberRunner {

}