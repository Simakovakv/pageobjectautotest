package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import other.TestProperties;

import java.util.concurrent.TimeUnit;

public class Init {
    static WebDriver driver;
    public static WebDriver getWebDriver(){
        return driver;
    }
    public static void startUp() {
       /* try {*/
            ChromeOptions options = new ChromeOptions();
        //options.addArguments("--headless");
        options.addArguments("--window-size=1920,1080");
        //options.addArguments("test-type");
        options.addArguments("disable-popup-blocking");
        options.addArguments("--disable-extensions");
        //options.addArguments("incognito");
       /* DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);*/
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        driver.get((String) TestProperties.getInstance().get("base.url"));

        /*    } catch(Exception e){startUp();}*/
    }

    public static void shutDown(){
        driver.close();
    }
}
