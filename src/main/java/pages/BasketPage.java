package pages;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class BasketPage extends MainPage{
    @FindBy(xpath="//*[@class = 'eCartSplitItems']//div[@class='eCartItem_name']/parent::*")
    List<WebElement> basketItems;

    @FindBy(xpath="//*[@class=\"eCartControls_buttons\"]//div[contains(text(), 'Удалить')]")
    List<WebElement> removeButton;

    String removeButtonFormat = "//*[@class=\"jsViewCollection jsChild_DOM_split\"]/descendant::div[@class='eCartControls_buttons'][1]";

    @FindBy (xpath = "//text()[contains(., 'Начать покупки')]//parent::a")
    WebElement buttonForCheckCleanBasket;

    @FindBy (className = "eCartTotal_summPrice ")
    List<WebElement> basketCost;
    @FindBy(className = "eCartControls_infoDate")
    WebElement baskerHeader;

    public BasketPage checkItems(){
        Assert.assertEquals("Elements are not equals", basketItems.size(), ProductPage.choosedItems.size());
        return this;
    }
    public BasketPage checkTotalCost(){
        if((basketCost.size()!=0))
            Assert.assertEquals("Elements are not equals", Integer.valueOf(basketCost.get(0).getText().replaceAll("\\D+", "")).intValue(), ProductPage.commonCost);
        return this;
    }
    public BasketPage deleteItems(){
        waitOfPage();
        stupidWait();
        try{
        while(removeButton.size() != 0) {
            waitOfElement(ExpectedConditions.elementToBeClickable(removeButton.get(0)));
            clickWithJS(removeButton.get(0));
        }}catch (StaleElementReferenceException e){
            System.out.println("nothing to delete");
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("nothing to delete");
        }
        ProductPage.commonCost = 0;
        ProductPage.choosedItems.clear();
        return this;
    }


}
