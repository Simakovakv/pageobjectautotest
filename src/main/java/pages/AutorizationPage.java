package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class AutorizationPage extends BasePage {
    @FindBy(xpath="//text()[contains(., 'Телефон')]/parent::span//following-sibling::input")
    WebElement phoneNumber;

    @FindBy(xpath="//text()[contains(., 'Войти')]/parent::button")
    WebElement submitButton;

    @FindBy(xpath="//text()[contains(., 'Почта')]/parent::span//following-sibling::input")
    WebElement emailInput;

    @FindBy(xpath="//text()[contains(., 'Пароль')]/parent::span//following-sibling::input")
    WebElement passInput;

    @FindBy(xpath = "//text()[contains(., 'Войти по почте')]/parent::a")
    WebElement loginFromEmail;

    @FindBy(xpath="//text()[contains(., 'Ксения')]//parent::*")
    WebElement loginName;


    public MainPage logInToOzon(String login, String password){
        clickWithJS(loginFromEmail);
        clickWithJS(emailInput);
        fillInputField(emailInput, login);
        click(passInput);
        fillInputField(passInput, password);
        clickWithJS(submitButton);
        //waitOfElement(ExpectedConditions.visibilityOf(loginName));
        //new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOf(loginName));
        return new MainPage();
    }






}
