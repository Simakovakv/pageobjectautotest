package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Function;
import static org.junit.Assert.assertNotEquals;

abstract class BasePage {
    WebDriver driver;
    public BasePage(){
        driver = Init.getWebDriver();
        PageFactory.initElements(driver, this);
    }
    public void click(WebElement element){
        scrollTo(element);
        element.click();
    }
    public void click(String xpath){
        click(driver.findElement(By.xpath(xpath)));
    }

    public void clickWithJS(WebElement element){
        scrollTo(element);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);

    }
    public void scrollTo(WebElement element){
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", element);
    }
    void waitOfElement(Function func){
        new WebDriverWait(driver, 10).until(func);
    }
    void waitOfPage(){
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

    void moveTo(WebElement element){
        new Actions(driver).moveToElement(element).build().perform();
    }

    public boolean checkVisibility(WebElement element){
        try {
            waitOfElement(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
     /*   if(element.isEnabled())
            return true;
        else {return false;}*/
    }

    public boolean checkEnabled(String xpath){
        return(driver.findElements(By.xpath(xpath)).size()!=0 ? true :false);
    }

    public void selectByText(WebElement element, String text){
        new Select(element).selectByVisibleText(text);
    }

    public void fillInputField(WebElement element, String text){
        waitOfPage();
        waitOfElement(ExpectedConditions.visibilityOf(element));
        //click(element);
        element.clear();
        stupidClear(element);
        element.sendKeys(text + "\n");
        //element.sendKeys(Keys.ENTER);
        waitOfPage();
    }
    public WebElement chooseFromList(String stringFormat, String itemName){
        By itemLocator = By.xpath(String.format(stringFormat, itemName));
        //List<WebElement> href = driver.findElements(By.xpath("//*[@id='desktopMenuMain']//li[@class='level-2__item']/a[contains(text(),'"+text+"')]"));
        waitOfElement(ExpectedConditions.visibilityOfAllElements(driver.findElements(itemLocator)));
        List<WebElement> href = driver.findElements(itemLocator);
        assertNotEquals(String.format("%s item is not found", itemName), href.size(), 0 );
        return href.get(0);
    }
    public WebElement chooseFromList(String xpath){
        return chooseFromList(xpath, "");
    }

    public WebElement constructFromStringFormat(String xpath){
        return driver.findElement(By.xpath(xpath));
    }

    public void refreshPage(){
        driver.navigate().refresh();
        waitOfPage();
    }
    public void stupidClear(WebElement element){
        Actions navigator = new Actions(driver);
        navigator.click(element)
                .sendKeys(Keys.END)
                .keyDown(Keys.SHIFT)
                .sendKeys(Keys.HOME)
                .keyUp(Keys.SHIFT)
                .sendKeys(Keys.BACK_SPACE)
                .perform();
    }

    public void stupidWait(){
        try {
            Thread.sleep(10000);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }


}
