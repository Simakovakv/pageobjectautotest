package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class ProductPage extends MainPage{
    @FindBy (xpath = "//text()[contains(., \"iPhone 7 Plus/8 Plus Black\")]/parent::h1")
    WebElement header;

    @FindBy (xpath = "//*[contains(text(), 'Вход')]")
    WebElement signInButton;

    @FindBy (className = "item-view")
    List<WebElement> items;

    @FindBy (xpath = "//span[contains(text(), 'Корзина')]")
    WebElement basketButton;

    @FindBy (xpath = ".//button[*[contains(text(), 'корзину')]]")
    WebElement addButton;
    String addButtonFormat = ".//button[*[contains(text(), 'корзину')]]";
    String priceFormat = ".//*[@class='price-number']/span";

    static List<WebElement> choosedItems = new ArrayList<>();
    static int commonCost = 0;

    public MainPage addItems(int count){
       waitOfPage();
        stupidWait();
     //   waitOfElement(ExpectedConditions.visibilityOf(items.get(0)));
    try {
        items.forEach(item -> {
            while (choosedItems.size() < count) {
                if (checkEnabled(addButtonFormat) && !(item.getText().trim().length() == 0)) {
                    WebElement button = item.findElement(By.xpath(addButtonFormat));
                    WebElement price = item.findElement(By.xpath(priceFormat));
                    commonCost += Integer.valueOf(price.getText().replaceAll(" ", "")).intValue();
                    clickWithJS(button);
                    choosedItems.add(item);
                    break;
                } else break;
            }
        });
    }
    catch(NoSuchElementException e){
        System.out.println("nothing to add, all items are not for sold");
    }
        return new MainPage();
    }

}
