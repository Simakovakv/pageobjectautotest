package pages;

//import cucumber.api.java.ru.Когда;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class MainPage extends BasePage{
    @FindBy(xpath = "//input[@placeholder=\"Выбирайте...\"]")
    WebElement inputSearchBlock;

    @FindBy(className = "search-button-wrap")
    WebElement searchButton;

    @FindBy(xpath = "//text()[contains(.,'iphone')]//ancestor::div/span[1]")
    WebElement itemFromDropbox;

    @FindBy (xpath = "//text()[contains(., \"iPhone 7 Plus/8 Plus Black\")]/parent::h1")
    WebElement header;

    @FindBy (xpath = "//*[contains(text(), 'Мой OZON')]/parent::*")
    WebElement userMenu;

    @FindBy (xpath = "//*[contains(text(), 'Вход')]")
    WebElement signInButton;

    @FindBy (xpath = "//div[@class='item-wrapper']/div[@class='item-view']")
    List<WebElement> items;

    @FindBy (xpath = "//text()[contains(., 'Корзина')]/parent::span")
    List<WebElement> basketButton;

    @FindBy (xpath = "//text()[contains(., 'Выйти')]//parent::div")
    WebElement signOutButton;

    @FindBy(xpath="//div[contains(text(), 'Ксения')]/parent::*")
    WebElement loginName;

    @FindBy(xpath = "//*[contains(text(), 'Посмотреть все') and ancestor::*[@class='filter']/*[contains(text(),'Бренды')]]")
            WebElement brendMoreButton;

    @FindBy(xpath = "//input[@class='input' and ancestor::div[@class='filter' and child::*[contains(text(), 'Бренды')]]]")
            WebElement inputBrandName;
    String nameBrandFormat = "//*[@class='search-show']//*[*[contains(text(), '%s')]]";

    String categoryMenuFormat = "//a[contains(text(), '%s') and ancestor::*[@class='main-menu']]";

    String subCategoryFormat = "//a[contains(text(), '%s') and parent::li]";
    List<WebElement> choosedItems = new ArrayList<>();
    int commonCost = 0;
    @FindBy(xpath = "//ul//li[@class = 'category-item' and contains(text(), '')]")
    WebElement subBut;

    @FindBy(xpath = "//div[*[contains(text(), 'Цена')]]//descendant::input[preceding-sibling::*[contains(text(), 'от')]]")
    WebElement inputFieldPriceStart;


    public AutorizationPage goToAutorized(){
        if(!checkVisibility(userMenu))
            refreshPage();
        moveTo(userMenu);
        clickWithJS(signInButton);
        return new AutorizationPage();
    }
    public MainPage logOutFromOzon(){
        checkVisibility(loginName);
        moveTo(loginName);
        click(signOutButton);
        return this;
    }

    public BasketPage goToBasket(){
        if(basketButton.size()!=0)
            refreshPage();
        click(basketButton.get(0));
        return new BasketPage();
    }

    public void isSearchBarDisplayed() {
        if(checkVisibility(inputSearchBlock))
            Assert.assertTrue(this.inputSearchBlock.isDisplayed());
    }
    public boolean isDisplayed(){
        return(userMenu.isDisplayed() ? true : false);
    }

    public ProductPage enterSearchRequest(String text) {
        fillInputField(inputSearchBlock, text+"\n");
        return new ProductPage();
    }

    public ProductPage chooseFromSearchDropbox() {
        click(itemFromDropbox);
        return new ProductPage();
    }


    public MainPage chooseCategoryMenu(String categoryName) {
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
        WebElement href = constructFromStringFormat(String.format(categoryMenuFormat, categoryName));
        //scrollTo(href);
        clickWithJS(href);
        return this;
    }


    public MainPage chooseSubCategory(String subCategoryName){
        WebElement href = constructFromStringFormat(String.format(subCategoryFormat, subCategoryName));
        scrollTo(href);
        click(href);
        return this;
    }

    public MainPage clickOneMoreBrendButton(){
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
        clickWithJS(brendMoreButton);
        return this;
    }

    public MainPage selectBrand(String nameBrand){
        fillInputField(inputBrandName, nameBrand);
        //clickWithJS(constructFromStringFormat(String.format(nameBrandFormat, nameBrand)));
        return this;
    }

    public MainPage fillInputFieldFrom(int price){
        fillInputField(inputFieldPriceStart, String.valueOf(price));
        return this;
    }
    }






