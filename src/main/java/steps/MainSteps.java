package steps;

//import cucumber.api.java.ru.Когда;
import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.AutorizationPage;
import pages.BasketPage;
import pages.MainPage;
import pages.ProductPage;

public class MainSteps {
    MainPage mainPage;
    ProductPage productPage;
    BasketPage basketPage;
    AutorizationPage autorizationPage;

    @Step("переход на ozozn.ru")
    public MainSteps goToTheSource() {
        mainPage = new MainPage();
        mainPage.refreshPage();
        return this;
    }

    //@Когда("пользователь авторизуется используя логин \"(.*)\" и пароль \"(.*)\"")
    @Step ("Авторизация на сервисе")
    public MainSteps loginToService() {
        autorizationPage = mainPage.goToAutorized();
        autorizationPage.logInToOzon("simakova.kv@gmail.com", "simka1991simka");
        return this;
    }
    @Step ("Поиск продукта по занчению ")
    public MainSteps searchProduct() {
        mainPage.enterSearchRequest("iPhone 7 Plus/8 Plus Black");
        return this;
    }
    @Step ("Добавление продуктов в корзину")
    public MainSteps addItemsToBasket(int count) {
        productPage = new ProductPage();
        productPage.addItems(count);
        return this;
    }
    @Step ("Переход в корзину и проверка наличия товаров")
    public MainSteps goToBasketAndCheckItems() {
        basketPage = mainPage.goToBasket();
        basketPage.checkItems()
                .checkTotalCost();
        return this;
    }
    public BasketPage checkItems(){
        basketPage.checkItems();
        return new BasketPage();
    }


    @Step ("Удаление продуктов")
    public MainSteps deleteAllItems() {
        basketPage.deleteItems();
        return this;
    }

    @Step ("Выход из учетной записи")
    public MainSteps logoutFromService() {
        mainPage.logOutFromOzon();
        return this;
    }

    @Step ("Выбор категории из меню")
    public MainSteps chooseCategoryFromMenu(String category, String subcategory){
        mainPage.chooseCategoryMenu(category)
        .chooseSubCategory(subcategory);
        return this;
    }
    @Step("Нажать кнопку Показать больше")
    public MainSteps clickOneMoreButton() {
        mainPage.clickOneMoreBrendButton();
        return this;
    }
    @Step("Выбор фильтра по бренду Apple")
    public MainSteps chooseBrendOfItem(String nameBrand) {
        mainPage.selectBrand(nameBrand);
        return this;
    }

@Step
public MainSteps enterToFieldStartPrice(int price){
    mainPage.fillInputFieldFrom(price);
    return this;
    }

}
