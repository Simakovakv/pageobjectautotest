package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.AutorizationPage;
import pages.Init;
import pages.MainPage;


public class MyStepdefs {
    AutorizationPage autorizationPage;
    MainPage mainPage;
    @Given("^user is on homepage$")
    public void user_is_on_homepage()  {
        Init.startUp();
    }
    @When("^user navigates to Login Page$")
    public void user_navigates_to_login_page() {
        autorizationPage = mainPage.goToAutorized();
    }
    @When("^user enter username and pass$")
    public void user_enter_name_and_pass() {
        autorizationPage.logInToOzon("simakova.kv@gmail.com", "simka1991simka");
    }
    @Then("^user return to main page$")
    public void user_return_to_main_page(){
        Assert.assertTrue(mainPage.isDisplayed());
    }
}
